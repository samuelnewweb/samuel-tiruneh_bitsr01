This is my first website project, and I am excited to share it with you! 
As a beginner, I am still learning the basics of web development, so please
bear with me as I work on improving my skills. 

My website is a simple one-page site that showcases some of my interests and hobbies. 

For this project, I used HTML,CSS and a little bit of JavaScript to create the website. I am still learning these technologies,so the code may not be perfect, but I am proud of what I have accomplished so far.

If you have any feedback or suggestions for how I can improve this website, please let me know! I am always looking for ways to learn and grow as a web developer. You can reach me at (http://mailto:samuelnewweb@gmail.com/).
